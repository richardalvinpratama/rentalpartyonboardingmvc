﻿namespace OnBoardingPartyRentMVC.Data.Enum
{
    public enum StoreLocationCategory
    {
        Jakarta,
        Jambi,
        Jawa,
        Kalimantan,
        Bali,
        Papua
    }
}
