﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace OnBoardingPartyRentMVC.Models
{
    public class AppUser : IdentityUser
    {
        [Required]
        [StringLength(50, MinimumLength = 6)]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;

        public virtual ICollection<Cart> Carts { get; set; }
    }
}
