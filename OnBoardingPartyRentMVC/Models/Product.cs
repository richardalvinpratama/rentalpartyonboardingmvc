﻿using OnBoardingPartyRentMVC.Data.Enum;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnBoardingPartyRentMVC.Models
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Store")]
        public int StoreId { get; set; } //will recognize as foreign id
        [Required]
        public string Name { get; set; }
        public string Desc { get; set; }
        public string Image { get; set; }
        public ProductCategory ProductCategory { get; set; }
        public int Stock { get; set; }
        public int Price { get; set; }
        public DateTime CreatedDateTime { get; set; } = DateTime.Now;

        public virtual ICollection<Cart> Carts { get; set; }
    }
}
