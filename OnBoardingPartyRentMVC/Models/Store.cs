﻿using System.ComponentModel.DataAnnotations;

namespace OnBoardingPartyRentMVC.Models
{
    public class Store
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Location { get; set; }

        public DateTime CreatedDateTime { get; set; } = DateTime.Now;

        public virtual ICollection<Product> Products { get; set; }
    }
}
