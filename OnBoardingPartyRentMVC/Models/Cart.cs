﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OnBoardingPartyRentMVC.Models
{
    public class Cart
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Appuser")]
        public int AppUserId { get; set; }
        [ForeignKey("Product")]
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public DateTime LastUpdateTime { get; set; } = DateTime.Now;
    }
}
