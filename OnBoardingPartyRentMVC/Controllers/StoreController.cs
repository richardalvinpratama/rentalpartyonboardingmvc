﻿using Microsoft.AspNetCore.Mvc;
using OnBoardingPartyRentMVC.Data;
using OnBoardingPartyRentMVC.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace OnBoardingPartyRentMVC.Controllers
{
    public class StoreController : Controller
    {
        private readonly ApplicationDbContext _db;
        public StoreController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IndexAdmin(string sortOrder, string searchString)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            var store = from s in _db.Stores
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                store = store.Where(s => s.Name.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    store = store.OrderByDescending(s => s.Name);
                    break;
                case "Date":
                    store = store.OrderBy(s => s.CreatedDateTime);
                    break;
                case "date_desc":
                    store = store.OrderByDescending(s => s.CreatedDateTime);
                    break;
                default:
                    store = store.OrderBy(s => s.Name);
                    break;
            }
            return View(store.ToList());
            //IEnumerable<Store> objStoreList = _db.Stores;
            //return View(objStoreList);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Store obj)
        {
            if (ModelState.IsValid)
            {
                return View(obj);
            }
            _db.Stores.Add(obj);
            _db.SaveChanges();
            TempData["success"] = "Store Added successfully!";
            return RedirectToAction("IndexAdmin");
        }

        public IActionResult Update(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var storeFromDb = _db.Stores.Find(id);

            if (storeFromDb == null)
            {
                return NotFound();
            }
            return View(storeFromDb);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(Store obj)
        {
            if (ModelState.IsValid)
            {
                return View(obj);
            }
            _db.Stores.Update(obj);
            _db.SaveChanges();
            TempData["success"] = "Store update successfully!";
            return RedirectToAction("IndexAdmin");
        }

        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }

            var obj = _db.Stores.Find(id);
            if (obj == null)
            {
                return NotFound();
            }
            _db.Stores.Remove(obj);
            _db.SaveChanges();
            TempData["success"] = "Store delete successfully!";
            return RedirectToAction("IndexAdmin");
        }
    }
}
