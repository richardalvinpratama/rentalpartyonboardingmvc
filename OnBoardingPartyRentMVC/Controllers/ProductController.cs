﻿using Microsoft.AspNetCore.Mvc;
using OnBoardingPartyRentMVC.Data;
using OnBoardingPartyRentMVC.Models;

namespace OnBoardingPartyRentMVC.Controllers
{
    public class ProductController : Controller
    {
        private readonly ApplicationDbContext _db;
        public ProductController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IndexAdmin()
        {
            IEnumerable<Product> objProductList = _db.Products;
            return View(objProductList);
        }
    }
}
