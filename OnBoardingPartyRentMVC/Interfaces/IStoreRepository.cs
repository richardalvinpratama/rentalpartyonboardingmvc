﻿using OnBoardingPartyRentMVC.Models;

namespace OnBoardingPartyRentMVC.Interfaces
{
    public interface IStoreRepository
    {
        Task<IEnumerable<Store>> GetAll();
        Store GetByIdAsync(int? id); //for to get detail store in controller

        bool Add(Store store);
        bool Update(Store store);
        bool Delete(Store store);
        bool Save();
        public Store SearchbyName(string searchString);
    }
}
