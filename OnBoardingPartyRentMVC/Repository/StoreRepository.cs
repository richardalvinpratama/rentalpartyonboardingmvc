﻿using Microsoft.EntityFrameworkCore;
using OnBoardingPartyRentMVC.Data;
using OnBoardingPartyRentMVC.Interfaces;
using OnBoardingPartyRentMVC.Models;

namespace OnBoardingPartyRentMVC.Repository
{
    public class StoreRepository : IStoreRepository
    {
        private readonly ApplicationDbContext _db;
        public StoreRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public async Task<IEnumerable<Store>> GetAll()
        {
            return await _db.Stores.ToListAsync();
        }

        public Store GetByIdAsync(int? id)
        {
            return _db.Stores.Find(id);
        } 

        public bool Add(Store store)
        {
            _db.Add(store);
            return Save();
        }

        public bool Update(Store store)
        {
            _db.Update(store);
            return Save();
        }

        public bool Delete(Store store)
        {
            _db.Remove(store);
            return Save();
        }

        public bool Save()
        {
            var saved = _db.SaveChanges();
            return saved > 0 ? true : false;
        }

        public Store SearchbyName(string searchString)
        {
            var store = from s in _db.Stores select s;

            if (!string.IsNullOrEmpty(searchString))
            {
                IEnumerable<Store> objLocationStoreLists = store.Where(s => s.Location!.Contains(searchString));
                return (Store)objLocationStoreLists;
            }
            return null;
        }
    }
}
